# Objectives

The objectives of this repo are:
* write an app in angular
* write some tests
* create a pipeline in jenkins
    - rise up a docker container with node + angular-cli
    - execute angular tests

## Create the Angular app
To create an angular app from scrach we need first set up the environment and install different packages like NodeJs and Angular-cli
### Set up environment
* First update
```
sudo apt-get update
```
* Install **node.js**, check node version at least 6.9.x to execute angular apps
```
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
```
* Install **npm** at least 3.x.x
```
sudo apt-get install npm
```
* Install **angular-cli**. It takes some time so be patient, the package will be saved by default in our *dependencies*. To [know more about](https://github.com/angular/angular-cli) ```angular-cli```. To [know more about](https://docs.npmjs.com/cli/install) ```npm install```
```
npm install @angular/cli
```
* Install dependencies.
```
cd my-app
npm install
```

Now we have everything configured, with our new default app *my-app*. By default the project already comes with some tests. We can run it like:
```
cd my-app
ng test
```
Or execut the app and test it locally:
* Serve the app. The ```ng serve``` command launches the server, watches your files, and rebuilds the app as you make changes to those files. To [know more about](https://github.com/angular/angular-cli/blob/master/docs/documentation/serve.md) ```ng serve```.
```
cd my-app
ng serve --open
```
### Test configuration
There are some files that config the process to run the tests.

1. **package.json** describes the command *test* to run ``` ng test```. 
    
    ```
    "scripts": {
        "test": "ng test"
    }
    ```
2. **angular-cli.json** defines which file to execute when command ```ng test``` is done.
    
    ```
    "apps": [
        {
            ...
            "test": "test.ts",
            ...
        }
    ],
    ```
3. **angular-cli.json** describes where the karma configuration file (```karma.conf.js```) is located.
    
    ```
    "test": {
        "karma": {
            "config": "./karma.conf.js"
        }
    },
    ```

## Jenkinsfile
This file is the script that contains the jenkins pipelines, so when creating a pipeline from jenkins, we must choose to get the Jenkinsfile from the Git repo:
* Checkout the [git repo](https://bitbucket.org/oski_02/simpleangulartests)
```
stage('Checkout') {
    steps {
        echo 'Checking out the repo...'
        checkout([$class: 'GitSCM', 
            branches: [[name: '*/master']], 
            doGenerateSubmoduleConfigurations: false, 
            extensions: [], 
            submoduleCfg: [], 
            userRemoteConfigs: [
                [url: 'https://oski_02@bitbucket.org/oski_02/simpleangulartests.git']
            ]
        ])
    }
}
```

* Run a docker image with both the node and angular-cli packages and execute the sh scripts
```
stage('Angular-cli Container') {
    agent {
        docker { 
            image 'teracy/angular-cli'
        }
    }
    steps {
        sh 'node -v'
        sh 'cd my-app && pwd && npm install && ng test'
    }
}
```
**NOTES**:
* About executing the sh scripts, every time you write new *sh* command, it will start new command prompt form ${WORKSPACE}, so instead of:
    ```
    sh 'cd my-app'
    sh 'npm install'
    ```
    Write it like:
    ```
    sh 'cd my-app && npm install'
    ```
* By default Chrome tests did not work within a Docker container, you need to configure karma and protractor for running tests within the Docker container.
See:
    - [karma.conf](https://github.com/teracyhq/angular-boilerplate/blob/master/karma.conf.js#L42)

    - [protractor.conf](https://github.com/teracyhq/angular-boilerplate/blob/master/protractor.conf.js#L13)

```--no-sandbox``` option must be used for Chrome to get it running within the Docker container.
And to run unit tests: ```$ ng test --browsers Chrome_no_sandbox```, e2e tests: ```$ ng e2e```

Another configuration could be to modify the *karma.conf.json*:
```
config.set({
    ...
    browsers: ['ChromeHeadlessNoSandbox'],
        customLaunchers: {
        ChromeHeadlessNoSandbox: {
            base: 'ChromeHeadless',
            flags: ['--no-sandbox']
        }
        },
    ...
});
````